"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fileController_1 = require("../controllers/fileController");
const multer = require("multer");
const path = require("path");
const fs = require("fs");
const DIR = '/home/aspire1086/Desktop/fileupload-2/node-ts/uploads';
let storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR);
    },
    filename: (req, file, cb) => {
        fs.readdir(DIR, (err, files) => {
            if (files.includes(file.originalname)) {
                cb(null, path.basename(file.originalname, path.extname(file.originalname)) + '-' + Date.now() + path.extname(file.originalname));
            }
            else {
                cb(null, file.originalname);
            }
        });
        // cb(null, path.basename(file.originalname, path.extname(file.originalname))+'-' + Date.now() + path.extname(file.originalname));
    }
});
const upload = multer({ storage: storage });
class Routes {
    constructor() {
        this.fileController = new fileController_1.FileController();
    }
    routes(app) {
        app.route('/api/upload')
            .post(upload.single('file'), this.fileController.uploadFile)
            .get(this.fileController.getAllFiles);
        app.get('/api/upload/:file(*)', this.fileController.downloadFile);
    }
}
exports.Routes = Routes;
