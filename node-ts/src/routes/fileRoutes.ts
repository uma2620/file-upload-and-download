import { FileController } from '../controllers/fileController';
import {Request} from 'express';
import * as multer from 'multer';
import * as path from 'path';
import * as fs from 'fs';

const DIR ='/home/aspire1086/Desktop/fileupload-2/node-ts/uploads';
        let storage = multer.diskStorage({
            destination: (req:Request, file:any, cb:any) => {

              cb(null, DIR);
            },
            filename: (req:Request, file:any, cb:any) => {
              fs.readdir(DIR, (err, files) => {
                 if(files.includes(file.originalname))          {
                 cb(null, path.basename(file.originalname, path.extname(file.originalname))+'-' + Date.now() + path.extname(file.originalname));

                 }
                 else{
                   cb(null,file.originalname);
                 }
           

              });

              // cb(null, path.basename(file.originalname, path.extname(file.originalname))+'-' + Date.now() + path.extname(file.originalname));
            }
        });
        const upload = multer({storage: storage });

export class Routes { 
    
    public fileController: FileController = new FileController() 
    
    public routes(app): void {   
        
        app.route('/api/upload')
        .post(upload.single('file'),this.fileController.uploadFile)
        .get(this.fileController.getAllFiles)
        app.get('/api/upload/:file(*)',this.fileController.downloadFile)
    }
}
