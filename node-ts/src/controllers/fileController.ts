import { Request, Response } from 'express';
import * as path from 'path';
import * as fs from 'fs';

export class FileController {

  public uploadFile(req: Request, res: Response) {
    console.log(req.file);
    if (!req.file) {
      console.log("No file received");
      res.json({
        success: false
      });
    } else {
      console.log('file received successfully');
      const successMessage = {
        status: "success",
        path: `http://172.24.145.225:5000/api/upload/${req.file.filename}`,
        filename: `${req.file.filename}`
      };
      res.status(200).send(successMessage)
    }
  }

  public downloadFile(req: Request, res: Response) {
    var file = req.params.file;
    var fileLocation = path.join('/home/aspire1086/Desktop/fileupload-2/node-ts/uploads', file);
    console.log(fileLocation);
    res.download(fileLocation, file);
  }
  public getAllFiles(req: Request, res: Response) {
    const testFolder = '/home/aspire1086/Desktop/fileupload-2/node-ts/uploads/';
    fs.readdir(testFolder, (err, files) => {
     
      var result: { filename: string; size: number;}[]=[];
      files.forEach(file => {
        var size = fs.statSync(testFolder+file).size;
        result.push({filename:file,size:size});
                
      });
      res.send(result)
    });

  }


}
