import { Component } from '@angular/core';
import { FileService } from './file.service';
import { environment } from 'src/environments/environment';
import { SizePipe } from './size.pipe';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = "upload"
  uploadedFiles: Array<File>;
  result: any;

  constructor(private fileservice: FileService) {
    this.result = { progress: 0 };

  }
  paths: any[];
  results: any;
  url: string;
  successMessage: string;
  success: boolean;
  errorMessage: string;

  ngOnInit() {
    this.url = environment.apiUrl;
    this.fileservice.getFiles().subscribe((response) => {

      this.results = response;
    });
  }

  fileChange(element) {
    this.uploadedFiles = element.target.files;
  }

  upload() {
    let formData = new FormData();
    for (var i = 0; i < this.uploadedFiles.length; i++) {
      formData.append("file", this.uploadedFiles[i], this.uploadedFiles[i].name);
    }
    this.fileservice.upload(formData).subscribe((response) => {
      console.log(response);
      this.success = true;
      this.result = response;
      setTimeout(() => {    //<<<---    using ()=> syntax
        this.successMessage = "File Uploaded sucessfully"
      }, 3000)
      setTimeout(() => {  
this.ngOnInit();        //<<<---    using ()=> syntax
      }, 5000);

     

    },
      (error) => {
        this.success = false;

        setTimeout(() => {    //<<<---    using ()=> syntax
          this.errorMessage = "Error Happened"
        }, 3000)

      });

  }
}
