import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpEvent, HttpEventType } from '@angular/common/http';
import {environment} from '../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  constructor(private http: HttpClient) {
  }
  upload(formData){
    return this.http.post(environment.apiUrl, formData, {
      reportProgress: true,
      observe: 'events'
    }).pipe(map((event) => {

      switch (event.type) {

        case HttpEventType.UploadProgress:
          let progress = Math.round(100 * event.loaded / event.total);
          return {  progress: progress };

        case HttpEventType.Response:

          return event.body;
        default:
          return `Unhandled event: ${event.type}`;
      }
    })
    );
          
  }
  getFiles(){
    return this.http.get(environment.apiUrl);
  }
}
